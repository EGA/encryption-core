/*
 *
 * Copyright 2020 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.encryption.core;

import org.junit.Test;
import uk.ac.ebi.ega.encryption.core.encryption.PgpPassword;
import uk.ac.ebi.ega.encryption.core.encryption.exceptions.AlgorithmInitializationException;
import uk.ac.ebi.ega.encryption.core.encryption.exceptions.WrongPassword;
import uk.ac.ebi.ega.encryption.core.exceptions.Md5CheckException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Files.readAllBytes;

public class PgpPasswordTest {

    private static final String PGP_ENCRYPTED_FILE_PATH = "/keyPairTest/keypair_with_passphrase_to_test_pgp_password/test_original_file_to_test_gpg_password.txt.gpg";
    private static final String PGP_PRIVATE_KEY_PATH = "/keyPairTest/keypair_with_passphrase_to_test_pgp_password/test_private_key_ascii_to_test_pgp_password.gpg.txt";
    private static final String PGP_PRIVATE_KEY_PASSPHRASE_PATH = "/keyPairTest/keypair_with_passphrase_to_test_pgp_password/test_passphrase_private_key_ascii_to_test_pgp_password.txt";
    private static final String PGP_ENCRYPTED_FILE_MD5_PATH = "/keyPairTest/keypair_with_passphrase_to_test_pgp_password/test_original_file_to_test_gpg_password.txt.gpg.md5";
    private static final String ORIGINAL_FILE_PLAIN_MD5_PATH = "/keyPairTest/keypair_with_passphrase_to_test_pgp_password/test_original_file_to_test_gpg_password.txt.md5";

    private static final String PGP_ENCRYPTED_FILE_PATH_EMPTY_PASSPHRASE = "/keyPairTest/keypair_empty_passphrase_to_test_pgp_password/test_original_file_to_test_gpg_password.txt.gpg";
    private static final String PGP_PRIVATE_KEY_PATH_EMPTY_PASSPHRASE = "/keyPairTest/keypair_empty_passphrase_to_test_pgp_password/test_private_key_ascii_empty_passphrase_to_test_pgp_password.gpg.txt";
    private static final String PGP_ENCRYPTED_FILE_MD5_PATH_EMPTY_PASSPHRASE = "/keyPairTest/keypair_empty_passphrase_to_test_pgp_password/test_original_file_to_test_gpg_password.txt.gpg.md5";
    private static final String ORIGINAL_FILE_PLAIN_MD5_PATH_EMPTY_PASSPHRASE = "/keyPairTest/keypair_empty_passphrase_to_test_pgp_password/test_original_file_to_test_gpg_password.txt.md5";

    private static final String PGP_ENCRYPTED_FILE_WITH_DIFFERENT_KEY_PATH = "/keyPairTest/test_file.txt2.gpg";

    @Test
    public void testDecryptEncryptedMd5() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_PATH).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH).toURI());
        final Path passphrase = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PASSPHRASE_PATH).toURI());
        final File encryptedMd5 = new File(this.getClass().getResource(PGP_ENCRYPTED_FILE_MD5_PATH).getFile());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.fileSource(passphrase)),
                    Md5Check.any(encryptedMd5),
                    Output.noOutput());
        }
    }

    @Test
    public void testDecryptDecryptedMd5() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_PATH).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH).toURI());
        final Path passphrase = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PASSPHRASE_PATH).toURI());
        final File plainMd5 = new File(this.getClass().getResource(ORIGINAL_FILE_PLAIN_MD5_PATH).getFile());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.fileSource(passphrase)),
                    Md5Check.any(plainMd5),
                    Output.noOutput());
        }
    }

    @Test(expected = Md5CheckException.class)
    public void testDecryptWrongMd5() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_PATH).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH).toURI());
        final Path passphrase = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PASSPHRASE_PATH).toURI());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.fileSource(passphrase)),
                    Md5Check.any("bad"),
                    Output.noOutput());
        }
    }

    @Test(expected = AlgorithmInitializationException.class)
    public void testDecryptFileEncryptedWithDifferentKey() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_WITH_DIFFERENT_KEY_PATH).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH).toURI());
        final Path passphrase = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PASSPHRASE_PATH).toURI());
        File encryptedMd5 = new File(this.getClass().getResource(PGP_ENCRYPTED_FILE_MD5_PATH).getFile());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.fileSource(passphrase)),
                    Md5Check.any(encryptedMd5),
                    Output.noOutput());
        }
    }

    @Test(expected = WrongPassword.class)
    public void testDecryptWrongPassphrase() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_PATH).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH).toURI());
        File encryptedMd5 = new File(this.getClass().getResource(PGP_ENCRYPTED_FILE_MD5_PATH).getFile());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.staticSource("bad_password".toCharArray())),
                    Md5Check.any(encryptedMd5),
                    Output.noOutput());
        }
    }

    //Test cases to test KeyPair generated with empty passphrase

    @Test
    public void testDecryptEncryptedMd5ForKeyPairWithEmptyPassphrase() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_PATH_EMPTY_PASSPHRASE).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH_EMPTY_PASSPHRASE).toURI());
        final File encryptedMd5 = new File(this.getClass().getResource(PGP_ENCRYPTED_FILE_MD5_PATH_EMPTY_PASSPHRASE).getFile());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.staticSource("".toCharArray())),
                    Md5Check.any(encryptedMd5),
                    Output.noOutput());
        }
    }

    @Test
    public void testDecryptDecryptedMd5ForKeyPairWithEmptyPassphrase() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_PATH_EMPTY_PASSPHRASE).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH_EMPTY_PASSPHRASE).toURI());
        final File plainMd5 = new File(this.getClass().getResource(ORIGINAL_FILE_PLAIN_MD5_PATH_EMPTY_PASSPHRASE).getFile());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.staticSource("".toCharArray())),
                    Md5Check.any(plainMd5),
                    Output.noOutput());
        }
    }

    @Test
    public void testDecryptWithAnyPassphraseForKeyPairWithEmptyPassphrase() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_PATH_EMPTY_PASSPHRASE).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH_EMPTY_PASSPHRASE).toURI());
        File encryptedMd5 = new File(this.getClass().getResource(PGP_ENCRYPTED_FILE_MD5_PATH_EMPTY_PASSPHRASE).getFile());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.staticSource("any_password".toCharArray())),
                    Md5Check.any(encryptedMd5),
                    Output.noOutput());
        }
    }

    @Test(expected = Md5CheckException.class)
    public void testDecryptWrongMd5ForKeyPairWithEmptyPassphrase() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_PATH_EMPTY_PASSPHRASE).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH_EMPTY_PASSPHRASE).toURI());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.staticSource("".toCharArray())),
                    Md5Check.any("bad"),
                    Output.noOutput());
        }
    }

    @Test(expected = AlgorithmInitializationException.class)
    public void testDecryptFileEncryptedWithDifferentKeyForKeyPairWithEmptyPassphrase() throws IOException, AlgorithmInitializationException, URISyntaxException, Md5CheckException {
        final EncryptionService service = new BaseEncryptionService();
        final URI encryptedFile = this.getClass().getResource(PGP_ENCRYPTED_FILE_WITH_DIFFERENT_KEY_PATH).toURI();
        final Path privateKeyPath = Paths.get(this.getClass().getResource(PGP_PRIVATE_KEY_PATH_EMPTY_PASSPHRASE).toURI());
        File encryptedMd5 = new File(this.getClass().getResource(PGP_ENCRYPTED_FILE_MD5_PATH_EMPTY_PASSPHRASE).getFile());

        try (final InputStream privateKeyInputStream = new ByteArrayInputStream(readAllBytes(privateKeyPath))) {
            service.encrypt(
                    Input.file(new File(encryptedFile),
                            new PgpPassword(privateKeyInputStream),
                            PasswordSource.staticSource("".toCharArray())),
                    Md5Check.any(encryptedMd5),
                    Output.noOutput());
        }
    }
}
