/*
 *
 * Copyright 2020 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.encryption.core.encryption;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ebi.ega.encryption.core.encryption.exceptions.AlgorithmInitializationException;
import uk.ac.ebi.ega.encryption.core.encryption.exceptions.WrongHeaderException;
import uk.ac.ebi.ega.encryption.core.encryption.exceptions.WrongPassword;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Security;
import java.util.Iterator;
import java.util.Optional;

import static org.bouncycastle.openpgp.PGPUtil.getDecoderStream;

public class PgpPassword implements EncryptionAlgorithm {

    private final static Logger LOGGER = LoggerFactory.getLogger(PgpPassword.class);

    private final InputStream privateKeyDataStream;

    public PgpPassword(final InputStream privateKeyDataStream) {
        this.privateKeyDataStream = privateKeyDataStream;
        installProviderIfNeeded();
    }

    @Override
    public OutputStream encrypt(final char[] password, final OutputStream outputStream) {
        throw new UnsupportedOperationException();
    }

    /**
     * Decrypts given PGP encrypted input stream.
     *
     * @param inputStream PGP encrypted data input stream.
     * @param password    private key passphrase.
     * @return Decrypted InputStream.
     * @throws AlgorithmInitializationException will be thrown in case operation fails.
     */
    @Override
    public InputStream decrypt(final InputStream inputStream, final char[] password) throws AlgorithmInitializationException {
        final PGPEncryptedDataList encryptedDataList = getPgpEncryptedDataList(inputStream)
                .orElseThrow(() -> new WrongHeaderException("No Pgp header found"));

        final Iterator<PGPEncryptedData> iterator = encryptedDataList.getEncryptedDataObjects();

        PGPPublicKeyEncryptedData encryptedPublicKeyData = null;

        if (iterator.hasNext()) {
            final PGPEncryptedData pgpEncryptedData = iterator.next();
            if (pgpEncryptedData instanceof PGPPublicKeyEncryptedData) {
                encryptedPublicKeyData = (PGPPublicKeyEncryptedData) pgpEncryptedData;
            }
        }

        if (encryptedPublicKeyData == null) {
            throw new RuntimeException("Unable to retrieve public key encrypted data");
        }

        final PGPSecretKey secretKey = getSecretKey()
                .orElseThrow(() -> new AlgorithmInitializationException("Can't find signing key in key ring"));
        final PGPPrivateKey privateKey = extractPrivateKey(secretKey, password)
                .orElseThrow(() -> new WrongPassword("Unable to retrieve private Key"));

        final Object message = getMessage(privateKey, encryptedPublicKeyData);

        if (message instanceof PGPLiteralData) {
            return ((PGPLiteralData) message).getInputStream();
        } else if (message instanceof PGPOnePassSignatureList) {
            throw new AlgorithmInitializationException("Encrypted message contains a signed message");
        } else {
            throw new AlgorithmInitializationException("Message type unknown");
        }
    }

    /**
     * Retrieves PGP secret key.
     *
     * @return Optional of PGPSecretKey in case of success or else empty optional.
     */
    private Optional<PGPSecretKey> getSecretKey() {
        try {
            final PGPSecretKeyRingCollection pgpSecretKeyRingCollection = new PGPSecretKeyRingCollection(
                    getDecoderStream(privateKeyDataStream), new JcaKeyFingerprintCalculator());
            final Iterator<PGPSecretKeyRing> keyRingIterator = pgpSecretKeyRingCollection.getKeyRings();

            while (keyRingIterator.hasNext()) {
                final PGPSecretKeyRing pgpSecretKeyRing = keyRingIterator.next();
                final Iterator<PGPSecretKey> keyIterator = pgpSecretKeyRing.getSecretKeys();
                while (keyIterator.hasNext()) {
                    final PGPSecretKey pgpSecretKey = keyIterator.next();
                    if (pgpSecretKey.isSigningKey()) {
                        return Optional.of(pgpSecretKey);
                    }
                }
            }
        } catch (PGPException | IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    /**
     * Extracts PGP private key.
     *
     * @param secretKey PGPSecretKey object.
     * @param password  private key passphrase.
     * @return Optional of PGPPrivateKey in case of success or else empty optional.
     */
    private Optional<PGPPrivateKey> extractPrivateKey(final PGPSecretKey secretKey, final char[] password) {
        try {
            return Optional.of(secretKey.extractPrivateKey(
                    new JcePBESecretKeyDecryptorBuilder()
                            .setProvider("BC")
                            .build(password)));
        } catch (PGPException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    /**
     * Retrieves the encrypted data list skipping any pgp header marker block if any.
     *
     * @param inputStream encrypted data input stream.
     * @return Optional of actual object or else empty optional.
     */
    private Optional<PGPEncryptedDataList> getPgpEncryptedDataList(final InputStream inputStream) {
        try {
            return Optional.of(skipHeaderMarker(new JcaPGPObjectFactory(getDecoderStream(inputStream))));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    private PGPEncryptedDataList skipHeaderMarker(final PGPObjectFactory pgpF) throws IOException {
        Object o = pgpF.nextObject();
        if (o instanceof PGPEncryptedDataList) {
            return (PGPEncryptedDataList) o;
        } else {
            return (PGPEncryptedDataList) pgpF.nextObject();
        }
    }

    private Object getMessage(final PGPPrivateKey privateKey, final PGPPublicKeyEncryptedData encryptedPublicKeyData)
            throws AlgorithmInitializationException {
        try {
            return decompressMessageIfNeeded(getClearData(privateKey, encryptedPublicKeyData));
        } catch (PGPException | IOException e) {
            throw new AlgorithmInitializationException("Unexpected exception", e);
        }
    }

    private InputStream getClearData(final PGPPrivateKey privateKey, final PGPPublicKeyEncryptedData encryptedPublicKeyData)
            throws AlgorithmInitializationException {
        try {
            return encryptedPublicKeyData
                    .getDataStream(new JcePublicKeyDataDecryptorFactoryBuilder()
                            .setProvider("BC")
                            .build(privateKey));
        } catch (PGPException e) {
            throw new AlgorithmInitializationException("Could not decrypt public key");
        }
    }

    private Object decompressMessageIfNeeded(final InputStream clear) throws PGPException, IOException {
        Object message = new JcaPGPObjectFactory(clear).nextObject();
        if (message instanceof PGPCompressedData) {
            PGPCompressedData cData = (PGPCompressedData) message;
            PGPObjectFactory pgpFact = new JcaPGPObjectFactory(cData.getDataStream());
            message = pgpFact.nextObject();
        }
        return message;
    }

    private void installProviderIfNeeded() {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }
}
