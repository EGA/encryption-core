/*
 *
 * Copyright 2020 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.encryption.core.encryption;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ebi.ega.encryption.core.encryption.exceptions.AlgorithmInitializationException;
import uk.ac.ebi.ega.encryption.core.encryption.exceptions.WrongHeaderException;
import uk.ac.ebi.ega.encryption.core.encryption.exceptions.WrongPassword;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Security;
import java.util.Iterator;
import java.util.Optional;

import static org.bouncycastle.openpgp.PGPUtil.getDecoderStream;

public class PgpKeyFiles implements EncryptionAlgorithm {

    private final static Logger LOGGER = LoggerFactory.getLogger(PgpKeyFiles.class);

    private final InputStream privateKey;

    public PgpKeyFiles(final InputStream privateKey) {
        this.privateKey = privateKey;
        installProviderIfNeeded();
    }

    @Override
    public OutputStream encrypt(final char[] password, final OutputStream outputStream) {
        throw new UnsupportedOperationException();
    }

    /**
     * Decrypts given PGP encrypted input stream.
     *
     * @param inputStream PGP encrypted data input stream.
     * @param password    private key passphrase.
     * @return Decrypted InputStream.
     * @throws AlgorithmInitializationException will be thrown in case operation fails.
     */
    @Override
    public InputStream decrypt(final InputStream inputStream, final char[] password) throws AlgorithmInitializationException {
        final PGPEncryptedDataList encryptedDataList = getPgpEncryptedDataList(inputStream)
                .orElseThrow(() -> new WrongHeaderException("No Pgp header found"));

        Iterator it = encryptedDataList.getEncryptedDataObjects();
        Optional<PGPPrivateKey> optionalPGPPrivateKey = Optional.empty();
        PGPPublicKeyEncryptedData publicKeyEncryptedData = null;
        PGPSecretKeyRingCollection pgpSecretKeyRingCollection = getPgpSecretKeyRingCollection();

        while (!optionalPGPPrivateKey.isPresent() && it.hasNext()) {
            publicKeyEncryptedData = (PGPPublicKeyEncryptedData) it.next();
            optionalPGPPrivateKey = findSecretKey(pgpSecretKeyRingCollection, publicKeyEncryptedData.getKeyID(), password);
        }
        final PGPPrivateKey pgpPrivateKey = optionalPGPPrivateKey
                .orElseThrow(() -> new AlgorithmInitializationException("Private key not found"));

        InputStream clearData = getClearData(publicKeyEncryptedData, pgpPrivateKey);

        JcaPGPObjectFactory plainObjectsFactory = new JcaPGPObjectFactory(clearData);

        Object message = decompressDataIfNeeded(plainObjectsFactory);
        if (message instanceof PGPLiteralData) {
            PGPLiteralData ld = (PGPLiteralData) message;
            return ld.getInputStream();
        } else if (message instanceof PGPOnePassSignatureList) {
            throw new AlgorithmInitializationException("Encrypted object contains a signed message");
        } else {
            throw new AlgorithmInitializationException("Unknown type of file");
        }
    }

    private Object decompressDataIfNeeded(JcaPGPObjectFactory plainObjectsFactory) throws AlgorithmInitializationException {
        try {
            Object message = plainObjectsFactory.nextObject();
            if (message instanceof PGPCompressedData) {
                PGPCompressedData cData = (PGPCompressedData) message;
                JcaPGPObjectFactory pgpFact = new JcaPGPObjectFactory(cData.getDataStream());
                message = pgpFact.nextObject();
            }
            return message;
        }catch (IOException e){
            throw new AlgorithmInitializationException("Error reading the pgp stream or parsing it", e);
        }catch (PGPException e){
            throw new AlgorithmInitializationException("Error decompressing pgp stream", e);
        }

    }

    private InputStream getClearData(PGPPublicKeyEncryptedData publicKeyEncryptedData, PGPPrivateKey pgpPrivateKey) throws AlgorithmInitializationException {
        try {
            return publicKeyEncryptedData.getDataStream(
                    new JcePublicKeyDataDecryptorFactoryBuilder().setProvider("BC").build(pgpPrivateKey));
        } catch (PGPException e) {
            throw new AlgorithmInitializationException("Error creating the clear data stream", e);
        }
    }

    private PGPSecretKeyRingCollection getPgpSecretKeyRingCollection() throws AlgorithmInitializationException {
        try {
            return new PGPSecretKeyRingCollection(getDecoderStream(privateKey), new JcaKeyFingerprintCalculator());
        } catch (IOException | PGPException e) {
            throw new AlgorithmInitializationException("Error reading private key", e);
        }
    }

    private Optional<PGPPrivateKey> findSecretKey(PGPSecretKeyRingCollection pgpSecretKeyRingCollection,
                                                  long keyID, char[] pass) throws AlgorithmInitializationException {
        try {
            PGPSecretKey pgpSecKey = pgpSecretKeyRingCollection.getSecretKey(keyID);
            if (pgpSecKey == null) {
                return Optional.empty();
            }
            return Optional.of(pgpSecKey.extractPrivateKey(
                    new JcePBESecretKeyDecryptorBuilder().setProvider("BC").build(pass)));
        } catch (PGPException e) {
            throw new WrongPassword("Password could not decrypt private key", e);
        }
    }

    /**
     * Retrieves the encrypted data list skipping any pgp header marker block if any.
     *
     * @param inputStream encrypted data input stream.
     * @return Optional of actual object or else empty optional.
     */
    private Optional<PGPEncryptedDataList> getPgpEncryptedDataList(final InputStream inputStream) {
        try {
            return Optional.of(skipHeaderMarker(new JcaPGPObjectFactory(getDecoderStream(inputStream))));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    private PGPEncryptedDataList skipHeaderMarker(final PGPObjectFactory pgpF) throws IOException {
        Object o = pgpF.nextObject();
        if (o instanceof PGPEncryptedDataList) {
            return (PGPEncryptedDataList) o;
        } else {
            return (PGPEncryptedDataList) pgpF.nextObject();
        }
    }

    private void installProviderIfNeeded() {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }
}
